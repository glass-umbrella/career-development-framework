# The Smithy Origin Story

## A Visual Timeline

<div><p><iframe src='https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1v2hPPn1QPh_iF2yYye_eShAxug2aQmGNxCdGWCz09RE&font=Default&lang=en&initial_zoom=2&height=650' width='100%' height='650' webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder='0'></iframe></p></div>

## A Humble Beginning

Inspired by the DevOps Dojo, The Smithy is the result of many experiments smelted down, reforged, and honed for success.

May we present, Founder and fellow Catalyst, Will Watkins . . .
<div style="position: relative; padding-bottom: 61.120543293718164%; height: 0;"><iframe src="https://www.loom.com/embed/63642d185a6a40ee99ab8f14fbdddf7f" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Our Mission

As the **catalysts in communities**,<br>
We splice our **culture** into the DNA of<br>
Our **customers**, **contributors**, and **partners**<br>
To **unlock competitive advantage** through **technology**<br>
By stewarding **platfrom ecosystems and communities** the **open source way**<br>

## Our Core Values

Our Core Values are modeled after the **Open Organization** and embody **transparency**, **inclusivity**, **adaptability**, **collaboration**, and **community**.

- The **Team** is the **Only**
- **Membership** requires **Contribution**
- **Trust** is the **Currency** between teams
- **Ownership** engenders **Autonomy**
- **Catalysts** carry **Glass Umbrellas**

<div class="embedly-embed-wrapper" contenteditable="false"><p>
<div class="three-dot-container"><br></div>
<div class="embedly-embed-overlay"><br></div>
<iframe class="embedly-embed" src="//cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2FrvyFYFXCj5c%3Fstart%3D14%26feature%3Doembed%26start%3D14&amp;display_name=YouTube&amp;url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DrvyFYFXCj5c&amp;image=https%3A%2F%2Fi.ytimg.com%2Fvi%2FrvyFYFXCj5c%2Fhqdefault.jpg&amp;key=41a4e606020248f79ec762cd55fbdda2&amp;type=text%2Fhtml&amp;schema=youtube" width="499" height="281" scrolling="no" title="YouTube embed" allowfullscreen="true" data-connected="1"></iframe>
</p></div>

For further information, we recommend:

  - [The Open Organization Book Series](https://opensource.com/open-organization/resources/book-series)

## Feedback
We welcome your [feedback](https://ozf4d0kv63i.typeform.com/to/e9kBIiWu)
