# Welcome to the Smithy

## Introduction
Welcome to your first waypoint on the journey to unlock new business value through technology!

<div><p><img src="https://glassumbrellapublicbucket.s3.eu-central-1.amazonaws.com/The+Smithy+-+Desktop.png" class="fr-fic fr-dib fr-bordered fr-rounded" style="width:800px;"></p></div>

**If software was eating the world ten years ago, then open source is devouring software today.**  This is a **continuous learning platform** for our community of customers, partners, and contributors to develop high performing **catalysts for change** within their organisations.  We advocate holistic transformation across **culture, ways of working, and technology** the open source way, and we drink our own champagne.  We know that it's the anvil, not the hammer, that shapes steel.  Join us and **let the sparks fly!**

## Feedback
We welcome your [feedback](https://ozf4d0kv63i.typeform.com/to/e9kBIiWu)

MKDocs is great for treating documentation as code!
This is a test site hosted on [GitLab Pages](https://pages.gitlab.io) and built using [MKDocs](https://mkdocs.org).  You can [browse its source code](https://gitlab.com/glass-umbrella/career-development-framework), fork it and start using it on your projects.
