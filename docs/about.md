# About Us
## Will Watkins

<div><p><img src="https://glassumbrellapublicbucket.s3.eu-central-1.amazonaws.com/profile_F22.jpeg" class="fr-fic fr-dib fr-bordered fr-rounded" style="width:800px;"></p></div>

**"Slow is smooth, smooth is fast!"**

Prior to Red Hat, I served in the United States Army as an Intelligence Officer. With an education in Islamic and Middle Eastern Studies and Computer Science from the University of Virginia, I focused on the intersection between different cultures and technology.  As a paratrooper, I led men and women in battle during both combat and peacetime and was awarded the Bronze Star Medal for leadership while deployed in Afghanistan in 2008 and 2010.  

After the military, I successfully transitioned my combat leadership, technical aptitude, and mission focus to the enterprise IT industry.  Drawing parallels from my counterinsurgency experience and the imminent threat to continued military dominance through modern software development, I advocate for organizational transformation across people, process, and technology to outpace disruption and optimize on opportunity.  My clients include major airframes, embedded weapon systems, cyberwarfare, cybersecurity firms, system integrators, and enterprise IT business systems.

In the summer of 2020, I moved to Munich, Germany to look after Red Hat's Transformation business across Europe, the Middle East, and Africa.

## [Insert Your Profile Here]

Please join our community via pull request!

## Feedback
We welcome your [feedback](https://ozf4d0kv63i.typeform.com/to/e9kBIiWu)

MKDocs is great for treating documentation as code!
This is a test site hosted on [GitLab Pages](https://pages.gitlab.io) and built using [MKDocs](https://mkdocs.org).  You can [browse its source code](https://gitlab.com/glass-umbrella/career-development-framework), fork it and start using it on your projects.
