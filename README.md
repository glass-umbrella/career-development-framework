<h1 align="center">
  <a href="https://github.com/ankitwasankar/mftool-java">
    <img src="https://gitlab.com/glass-umbrella/career-development-framework/-/raw/main/Images/anvil.jpg" alt="The Smithy">
  </a>
</h1>
<p align="center">

<a target="_blank" href="https://travis-ci.com/github/ankitwasankar/mftool-java"><img src="https://travis-ci.com/ankitwasankar/mftool-java.svg?branch=master" /></a>

<a target="_blank" href="https://github.com/ankitwasankar/mftool-java/blob/master/license.md"><img src="https://camo.githubusercontent.com/8298ac0a88a52618cd97ba4cba6f34f63dd224a22031f283b0fec41a892c82cf/68747470733a2f2f696d672e736869656c64732e696f2f707970692f6c2f73656c656e69756d2d776972652e737667" /></a>
<a target="_blank" href="https://www.linkedin.com/in/ankitwasankar/"><img height="20" src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" /></a>
</p>
<p align="center">
  This repository contains the <strong>SMITHY Docs</strong> source code.
 
</p>

<p align="center">
<a href="#introduction">Introduction</a> &nbsp;&bull;&nbsp;
<a href="#installation">Installation</a> &nbsp;&bull;&nbsp;
<a href="#usage">Usage</a> &nbsp;&bull;&nbsp;
<a href="#documentation">Documentation</a> &nbsp;&bull;&nbsp;
<a href="#issue">Issue?</a>
</p>

# Introduction


## Installation
##### 
```

```
##### 
```

```
sample link syntax
<a href="https://google.com</a>


## Usage
Sample code that shows how to use the documentation:<br/>
```

```
The other available methods are described in the next section.

## Documentation


### 1. How to initialize an MFTool object
```

```

```

```

```

```

### 2.
```

```

### 3. 
```

```

### 4. 
<b>...</b>
```


```




## Issue
This repository is maintained actively, so if you face any issue please <a href="https://gitlab.com/glass-umbrella/career-development-framework/-/issues">raise an issue</a>.

<h4>Liked the work ?</h4>
Give the repository a star :-)
